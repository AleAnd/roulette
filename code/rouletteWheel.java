import java.util.Random;

public class rouletteWheel{
    private Random random;
    private int number;
    
    public rouletteWheel(){
        this.random = new Random();
        this.number = 0;
    }

    public void spin(){
        this.number = random.nextInt(37);
    }

    public int getValue(){
        return this.number;
    }
}