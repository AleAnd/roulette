import java.util.Scanner;

public class Roulette{

    public static boolean checkIfUserWon(int numberUserBetOn, int winningNumber){
        if(numberUserBetOn == winningNumber){
            return true;
        } else
            return false;
        
    }
    public static void main(String[]args){
        boolean endGame = false;
        Scanner scanner = new Scanner(System.in);
        rouletteWheel RouletteWheel = new rouletteWheel();
        int player = 1000;

        while(endGame != true){
            
            int doesUserBet;
            do { 
                System.out.println("Input the number you would like to bet on (Input -1 if you would not like to bet)");
                doesUserBet = scanner.nextInt();
            } while (doesUserBet < -1 || doesUserBet >= 37);
            
            if(doesUserBet == -1){ 
                endGame = true;
                break;
            } else {
                int amountUserWantsToBet;
                System.out.println("This is how much $$ you have: " + player);
                
                do { 
                System.out.println("How much would you like to bet?");
                amountUserWantsToBet = scanner.nextInt();
                } while (amountUserWantsToBet < 0 || amountUserWantsToBet > player);
                
                player -= amountUserWantsToBet;
                RouletteWheel.spin();

                if(RouletteWheel.getValue() == doesUserBet){
                    int amountWon = amountUserWantsToBet * 35;
                    player += amountWon;
                    System.out.println("It spun a " + RouletteWheel.getValue());
                    System.out.println("Congratulations! You won " + amountWon + " dollars");
                } else {
                    System.out.println("It spun a " + RouletteWheel.getValue());
                    System.out.println("Unfortunately, you didn't win :/");
                }
                
            }
        }
    }
}